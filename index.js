
const addressTypes = [
	{icon: '🏢', title: 'Office'}, 
	{icon: '📧', title: 'Mailing'}, 
	{icon: '📍', title: 'Other'}
];

const maxZoom = 8;

const getAutocomplete = q=>fetch(`/api/place/autocomplete/json?input=${q}`).then(r=>r.json());

const getDetails = id=>fetch(`/api/place/details/json?placeid=${id}`).then(r=>r.json());

const getGeocode = q=>fetch(`/api/geocode/json?address=${q}`);// not used, it's similar to autocomplete, with just one result

// we should use a proxy on the same domain of course, and remove http://crossorigin.me, the proxy would also add the key -- done



const Dropdown = ({render, initialState, onClose=()=>{}})=>React.createClass({
	getInitialState(){ 
		return Object.assign({active:false},initialState);
	},
	shouldComponentUpdate(p, s){
		return !equals(s, this.state) || !equals(p, this.props); 
	},
	componentWillUnmount(){ // clean listeners there
		document.removeEventListener('mousedown', this.closeOnOut);
		document.removeEventListener('keydown', this.closeOnEscape);
	},
	componentDidUpdate(_, s){
		if (s.active === this.state.active) return;
		if (this.state.active){
			document.addEventListener('mousedown', this.closeOnOut);
			document.addEventListener('keydown', this.closeOnEscape);
		} else {
			document.removeEventListener('mousedown', this.closeOnOut);
			document.removeEventListener('keydown', this.closeOnEscape);
		}
	},
	closeOnOut(e){
		if (!this.refs.el.contains(e.target)){
			document.removeEventListener('mousedown', this.closeOnOut);
			document.removeEventListener('keydown', this.closeOnEscape);
			this.setState({active:false});
			onClose();
		}
	},
	closeOnEscape(e){
		if (e.keyCode===27){
			document.removeEventListener('mousedown', this.closeOnOut);
			document.removeEventListener('keydown', this.closeOnEscape);
			this.setState({active:false});
			onClose();
		}
	},
	render
});

const IconDropdown = Dropdown({
	render(){
		const {address} = this.props, {type}=address;
		return v('div', {ref:'el', className:'dropdown icons'+(this.state.active?' active':'')},
			v('span', {
					className:'icon', title:addressTypes[type].title,
					onClick: e=>this.setState({active:!this.state.active})
				}, addressTypes[type].icon),
			v('div',
				addressTypes.filter((_,j)=>j!==type).map(({icon, title},key)=>
					v('span', {
							className:'icon', key, title, 
							onMouseDown:e=>{
								this.props.update(address, {type:addressTypes.findIndex(a=>a.title===title)}, true);
								this.setState({active:false});
							}
						},
					icon)
				)
			)
		)
	}
});




const AddressDropdown = Dropdown({
	initialState: {predictions: []},
	onClose(){
		
	},
	render(){
		const {address, marker} = this.props, {value}=address;
		return v('div', {ref:'el', className:'dropdown address'+(this.state.active?' active':'')},
			v('address', {
				className:(this.state.dirty?'dirty':'')+(marker&&marker.iw.getMap()?' focus':''),
				contentEditable: true,//'plaintext-only', // FF sucks https://bugzilla.mozilla.org/show_bug.cgi?id=1289000
				dangerouslySetInnerHTML:{__html: value},
				onFocus: e=>{
					if (this.state.dirty) this.setState({active:true});
					this.props.toggleMarkers(this.props.marker);
					// marker.setIcon('http://maps.google.com/mapfiles/ms/icons/red-dot.png');
					// marker.setAnimation(google.maps.Animation.BOUNCE);
					// setTimeout(_=>{
					// 	marker.setAnimation(null);
					// }, 700) // google maps animations are terrible... there must be a better css way
				},
				onBlur:e=>{
					this.props.marker.iw.close();
					// this.props.marker.setIcon('http://maps.google.com/mapfiles/ms/icons/purple-dot.png');
				},
				onKeyUp: ({currentTarget})=>{
					getAutocomplete(currentTarget.textContent).then(({predictions})=>{
						this.setState({predictions, active: true, dirty:currentTarget.textContent!==value});
					})
				}
			}),
			v('span', {onClick:this.props.delete, title:'Remove address'}),
			v('ul',
				this.state.predictions.map((p,key)=>v('li', {key,
						onMouseDown:e=>{
							this.props.update(address, {value:p.description});
							this.setState({active:false, dirty:false});

							getDetails(p.place_id).then(({result})=>{
								if (!result) return console.warn('failed to find place_id', p.place_id);
								const adr = {value:new String(result.formatted_address), location:result.geometry.location}
								this.props.update(address, adr, true, true);

							}).catch(console.error)
						}
					},
					v('span', {
						dangerouslySetInnerHTML:{__html: p.matched_substrings.reduceRight((a, c) => `${a.slice(0, c.offset)}<b>${a.slice(c.offset,c.offset+c.length)}</b>${a.slice(c.offset+c.length)}`, p.description)} // highlight matches with <b>
					}) 
				))
			)
		)
	}
});



const AddressList = React.createClass({

	shouldComponentUpdate(p, s){
		return !equals(s, this.state); // pure component, could use mixin
	},

	getInitialState(){
		return {
			addresses: this.props.addresses,
			markers: {}, // address id -> google maps marker  
			dragI: -1 // item index being dragged
		};
	},

	componentDidMount(){
		updateHeights(this.refs.list);
		const bounds = new google.maps.LatLngBounds();
		this.map = new google.maps.Map(this.refs.map, {
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				center: {lat: -34.397, lng: 150.644},
				zoom: 6
		});

		const {addresses} = this.state, markers={};
		for (let adr of addresses){
			const marker = new google.maps.Marker({
				map:this.map,
				position: adr.location,
				id: adr.id, // bit ugly
				iw: new google.maps.InfoWindow({content:adr.value}) // but well, practical
				// title: adr.value
				// icon: 'http://maps.google.com/mapfiles/ms/icons/purple-dot.png'
			});
			markers[adr.id] = marker;
			bounds.extend(marker.getPosition());
			marker.addListener('click', _=> this.toggleMarkers(marker,1))
		}
		this.setState({markers});
		this.map.fitBounds(bounds);
		if (this.map.getZoom()>maxZoom)	this.map.setZoom(maxZoom);

		if(navigator.geolocation && !addresses.length) {
			navigator.geolocation.getCurrentPosition(({coords:{latitude:lat, longitude:lng}})=>{
				this.map.setCenter({lat, lng})
			});
		}
		google.maps.event.addListener(this.map, 'click', _=>this.toggleMarkers(undefined, 4))//close all, because they are annoying
	},

	add(){
		const {addresses, markers} = this.state;
		const id = Math.max(...addresses.map(a=>a.id))+1;
		const marker = new google.maps.Marker({map:this.map, id, iw:new google.maps.InfoWindow()});
		marker.addListener('click', _=> this.toggleMarkers(marker, 2));
		this.setState({
			addresses:[{id, value:'', type:2}].concat(addresses),
			markers: Object.assign({}, markers, {[id]:marker})
		});
	},

	delete(address){
		const {addresses, markers} = this.state, i=addresses.indexOf(address);
		const addresses2=addresses.slice(0,i).concat(addresses.slice(i+1)), markers2=Object.assign({}, markers);
		markers2[address.id].setMap(null); // remove marker
		delete markers2[address.id];
		this.setState({addresses:addresses2, markers:markers2});
		const bounds = new google.maps.LatLngBounds();
		for (let id in markers2){
			if (markers2[id].getPosition()) 
				bounds.extend(markers2[id].getPosition());
		}
		this.map.fitBounds(bounds);
		if (this.map.getZoom()>maxZoom)	this.map.setZoom(maxZoom);
	},

	update(address/*former address*/, adr/*changes to do on it*/, persist/*persist changes in backend or not*/, fitBounds/*update maps bounds*/){
		const {addresses, markers} = this.state, i=addresses.indexOf(address);
		const adr2 = Object.assign({}, address, adr);
		this.setState({addresses:Object.assign(addresses.slice(), {[i]: adr2})}); // make a copy and set it at i-th position
		if (persist)
			this.persist().then(console.log, console.error);
		if (fitBounds){
			markers[adr2.id].setPosition(adr2.location);
			markers[adr2.id].iw.setContent(adr2.value+'');
			// markers[adr2.id].setTitle(adr2.value);
			const bounds = new google.maps.LatLngBounds();
			for (let id in markers){
				if (markers[id].getPosition()) 
					bounds.extend(markers[id].getPosition());
			}
			this.map.fitBounds(bounds);
			if (this.map.getZoom()>maxZoom)	this.map.setZoom(maxZoom); // repeated 3 times.. should factor
		}
	},
	persist(){ // persist address list in DB
		return fetch('/api/addresses', {
			method:'PUT', 
			headers:{'Content-Type': 'application/json'},
			body: JSON.stringify(this.state.addresses.filter(adr=>adr.value.trim()))
		})
	},

	dragOver(e, i){
		e.preventDefault();
		const {dragI, addresses} = this.state;
		if (dragI===i) return;
		const {top, bottom} = e.currentTarget.rect(),
			center = (top+bottom)/2;
		
		if (i===dragI+1 && e.clientY < center || i===dragI-1 && e.clientY > center) return;

		const addresses2 = addresses.slice(0,dragI).concat(addresses.slice(dragI+1)); // removing dragI
		const i2 = i - (dragI<i) + (e.clientY > center); // if dragI was before, we need to remove 1, if we drag after the center we add 1
		const addresses3 = addresses2.slice(0,i2).concat(addresses[dragI]).concat(addresses2.slice(i2));
		this.setState({addresses:addresses3, dragI:i2})
	},
	dragStart(e, i){
		this.setState({dragI:i});
		e.dataTransfer.setData('text/custom', 'sort');
	},
	// drop(e){
	// 	console.log('dropped');
	// },
	dragEnd(e){
		this.setState({dragI:-1});
		this.persist().then(console.log, console.error);
	},

	componentDidUpdate(p,s){
		updateHeights(this.refs.list);
	},

	toggleMarkers(marker, refreshList){ // open only markers[id]
		const {addresses, markers} = this.state, i=addresses.findIndex(adr=>adr.id==(marker&&marker.id));
		for (let id in markers)
			markers[id].iw.close();
		if (marker) {
			marker.iw.open(marker.getMap(),  marker);
		}
		if (refreshList){
			console.log('update marker->adr', i, marker, addresses);
			this.update(addresses[i], addresses[i])// force an update, to show focused items when clicking a marker
		}
		
	},

	render(){
		const {addresses, markers, dragI} = this.state;

		return v('div', {className:'modal'},
			v('div', {className:'modal-header'},
				v('button', {title: 'Add address', onMouseDown: this.add}, '➕'),
				v('h3', 'Addresses'),
				v('button', {title: 'Close', onMouseDown: e=>{}}, '❌')
			),
			v('div', {className:'modal-body'},
				v('div', {className: 'wrapper'},
					v('ul',{
							ref:'list', className:dragI>=0?'dragging':'', // for https://bugs.chromium.org/p/chromium/issues/detail?id=630726 small bug on chrome with li:hover and drag
							onDragEnd:dragI>=0&&this.dragEnd,
							onKeyUp:e=>updateHeights(e.currentTarget)
						},
						addresses.map((_,k)=>{
							const i=addresses.length-1-k, adr=addresses[i];
							return v('li', {key:adr.id, draggable:true, 
									onDragStart:e=>this.dragStart(e,i), 
									onDragOver:dragI>=0&&(e=>this.dragOver(e,i)),
									style: {opacity:dragI===i?.5:1}
								},
								v(IconDropdown, {address:adr, update:this.update}), // todo pass adr, this.update
								v(AddressDropdown, {address:adr, marker:markers[adr.id], toggleMarkers:this.toggleMarkers, update:this.update, delete:this.delete})
							)}
						)
					)
				),
				v('div',  {ref:'map'})
			)
		)
	}
})


// address {id, value='', type=2, location, marker} 

const initialAddresses = [
	{id:1, value:'Circle Hill Dr, San Jose, CA 95120, USA', type: 2, location:{lat: 37.2189558,
						lng: -121.8861579}},
	{id:2, value:'257 Whatcom Rd, Abbotsford, BC V3G 1Y8, Canada', type: 1, location: {lat: 49.005455,lng: -122.2256586}},
	{id:3, value:'77 A He Mai Ti Jiang Lu, Yining Shi, Yili Hasakezizhizhou, Xinjiang Weiwuerzizhiqu, China, 835000', type: 1, location: {lat: 43.915565, lng: 81.306247}}
];

ReactDOM.render(v(AddressList, {addresses: initialAddresses}), app)



/*
// An address is {street_number, route, locality, administrative_area_level_1, country, postal_code}

// displayed (with USA's convention)
// street_number route, locality, administrative_area_level_1 postal_code, country


// it's possible to use inputs like that, but took the choice to use API, it's more limited (you don"t have the fields below anymore)
var autocomplete = new google.maps.places.Autocomplete(someInput);

autocomplete.addListener('place_changed', _=>{
	var place = autocomplete.getPlace();
	console.log(place);
	for (var component in componentForm) {
		Object.assign(document.getElementById(component), {value: '', disabled: false});
	}

	for (let c of place.address_components) {
		const {types: [addressType], [componentForm[addressType]]:name} = c;
		Object.assign(document.getElementById(addressType)||{}, {value: name || ''});
	}
});

const componentForm = { // the config for google's autocomplete responses format
	street_number: 'short_name',
	route: 'long_name',
	locality: 'long_name',
	administrative_area_level_1: 'short_name',
	country: 'long_name',
	postal_code: 'short_name'
};
*/


// a few utils 
function v (tag, p, ...children){
	return !p || React.isValidElement(p)||typeof p==='string'||Array.isArray(p) ?
		React.createElement(tag, undefined, p, ...children) :
		React.createElement(tag, p, ...children);
}

function equals(o, o2){ //object equal only, and assume same keys
	for (var i in o)
		if (o[i]!==o2[i]) return false;
	return true;
}

function updateHeights(list){ // update heights for each <li> item, that are absolute positioned
	let y=0;
	for (let i=list.childElementCount-1, el; i>=0, el=list.children[i]; i--){
		el.style.transform = `translateY(${y}px)`;
		y+=8+el.offsetHeight;
	}
	list.style.height = y+'px';
}


function dropdownToggle({currentTarget}){
	const closeOnOut = e => {
		if (!currentTarget.parentNode.contains(e.target)){
			document.removeEventListener('mousedown', closeOnOut);
			document.removeEventListener('keydown', closeOnEscape);
		}
	}, closeOnEscape = e=> {
		if (e.keyCode===27){
			document.removeEventListener('mousedown', closeOnOut);
			document.removeEventListener('keydown', closeOnEscape);
		}
	}, toggle = (b=!currentTarget.classList.contains('selected')) => {
		if (b){
			document.addEventListener('mousedown', closeOnOut);
			document.addEventListener('keydown', closeOnEscape);
			// currentTarget.closest('li').style.zIndex=1; // it sucks, but transform creates a damn stacking layer // I reversed the order in updateHeights rather
		} else {
			document.removeEventListener('mousedown', closeOnOut);
			document.removeEventListener('keydown', closeOnEscape);
			// currentTarget.closest('li').style.zIndex='';
		}
		currentTarget.classList.toggle('selected', b);
	};
	toggle();
	return toggle;
}

// function dropdown(label, content, o){ 
// 	return v('div', Object.assign({className:'dropdown'}, o),
// 		label,
// 		content
// 	)
// }
// function dropdown({label, content, ref='el', style/*, ...o I want that!*/}){ 
// 	return v('div', {className:'dropdown', ref, style/*, ...o*/},
// 		label,
// 		content
// 	)
// }

Element.prototype.rect = Element.prototype.getBoundingClientRect;