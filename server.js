const express = require('express');
const request = require('request');

const app = express();
app.use(express.static('.'));
app.listen(3000);

app.put('/api/addresses', (req, res)=>{
	res.json({success:true})
});

app.get('/api/place/*', (req, res)=>{
	request('https://maps.googleapis.com/maps'+req.originalUrl+'&key=<mapKey>').pipe(res);
});